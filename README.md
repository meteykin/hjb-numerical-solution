# HJB numerical solution

This program computes the numerical solution of Hamilton–Jacobi–Bellman quasi-variational inequality. The output is a .csv file with found viscosity solution and the corresponding optimal stochastic and impulse controls for the market maker.