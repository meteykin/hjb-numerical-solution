#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>
#include <iterator>
#include <fstream>
#include <iomanip>


class Strategy {
public:
	Strategy(double a_T, double a_A, int a_Q,
		double a_k, double a_rho,
		double a_gamma_ask, double a_gamma_bid,
		double a_lambda_ask, double a_lambda_bid,
		double a_Delta, double a_sigma, double a_Upsilon,
		double a_phi, double a_psi, int a_n_a) :
		T(a_T), A(a_A), Q(a_Q), k(a_k), rho(a_rho),
		gamma_ask(a_gamma_ask), gamma_bid(a_gamma_bid),
		lambda_ask(a_lambda_ask), lambda_bid(a_lambda_bid),
		Delta(a_Delta), sigma(a_sigma), Upsilon(a_Upsilon),
		phi(a_phi), psi(a_psi), n_a(a_n_a)
	{
		n_t = static_cast<int>(T / 2 * ((n_a - 1) * k + (n_a - 1) *
			(n_a - 1) * rho * rho / A / A / 2 + lambda_ask + lambda_bid));
		
		d_alpha = 2 * A / (n_a - 1);
		d_t = T / n_t;
	}

	double boundaryCondition(int64_t alpha_idx, int64_t q) {
		if (q > 0) {
			return q * (-Upsilon - psi * q);
		}
		return q * (Upsilon - psi * q);
	}

	void Initialize() {
		h_values.push_back(std::vector<std::vector<double>>(2 * Q + 1,
			std::vector<double>(n_a)));

		for (int64_t alpha_idx = 0; alpha_idx < n_a; ++alpha_idx) {
			for (int64_t q = -Q; q <= Q; ++q) {
				h_values[0][q + Q][alpha_idx] = boundaryCondition(alpha_idx, q);
			}
		}
	}

	double h(int64_t alpha_idx, int64_t q) {
		if (alpha_idx >= n_a) {
			return h_values.back()[q + Q][n_a - 1] + (h_values.back()[q + Q][n_a - 1]
				- h_values.back()[q + Q][n_a - 2]) * (alpha_idx - n_a + 1);
		}

		if (alpha_idx < 0) {
			return h_values.back()[q + Q][0] - alpha_idx * (h_values.back()[q + Q][0]
				- h_values.back()[q + Q][1]);
		}

		return h_values.back()[q + Q][alpha_idx];
	}

	template <bool Sign>
	double I_operator(int64_t alpha_idx, int64_t q) {
		double gamma = Sign ? gamma_ask : gamma_bid;
		double frac = gamma / d_alpha;

		if (Sign) {
			return h(alpha_idx + std::floor(frac), q) + (frac - std::floor(frac))
				* (h(alpha_idx + std::ceil(frac), q) - h(alpha_idx + std::floor(frac), q));
		}

		return h(alpha_idx - std::floor(frac), q) - (frac - std::floor(frac))
			* (h(alpha_idx - std::ceil(frac), q) - h(alpha_idx - std::floor(frac), q));
	}

	double T_operator(int64_t alpha_idx, int64_t q) {
		double alpha_hat = -A + min(n_a - 1, max(0, alpha_idx)) * 2 * A / (n_a - 1);

		if (q + 1 > Q) {
			return h(alpha_idx, q) + d_t * (alpha_hat * sigma * q + k * max(0, -alpha_hat) / d_alpha
				* (h(alpha_idx + 1, q) - h(alpha_idx, q)) - k * max(0, alpha_hat) / d_alpha
				* (h(alpha_idx, q) - h(alpha_idx - 1, q)) + rho * rho / 2 / d_alpha / d_alpha
				* (h(alpha_idx + 1, q) - 2 * h(alpha_idx, q) - h(alpha_idx - 1, q))
				- phi * q * q + lambda_ask * max(Delta + I_operator<true>(alpha_idx, q - 1),
					I_operator<true>(alpha_idx, q)) + lambda_bid * I_operator<false>(alpha_idx, q)
				- (lambda_ask + lambda_bid) * h(alpha_idx, q));
		}

		if (q - 1 < -Q) {
			return h(alpha_idx, q) + d_t * (alpha_hat * sigma * q + k * max(0, -alpha_hat) / d_alpha
				* (h(alpha_idx + 1, q) - h(alpha_idx, q)) - k * max(0, alpha_hat) / d_alpha
				* (h(alpha_idx, q) - h(alpha_idx - 1, q)) + rho * rho / 2 / d_alpha / d_alpha
				* (h(alpha_idx + 1, q) - 2 * h(alpha_idx, q) - h(alpha_idx - 1, q))
				- phi * q * q + lambda_ask * I_operator<true>(alpha_idx, q)
				+ lambda_bid * max(Delta + I_operator<false>(alpha_idx, q + 1),
					I_operator<false>(alpha_idx, q)) - (lambda_ask + lambda_bid) * h(alpha_idx, q));
		}

		return h(alpha_idx, q) + d_t * (alpha_hat * sigma * q + k * max(0, -alpha_hat) / d_alpha
			* (h(alpha_idx + 1, q) - h(alpha_idx, q)) - k * max(0, alpha_hat) / d_alpha
			* (h(alpha_idx, q) - h(alpha_idx - 1, q)) + rho * rho / 2 / d_alpha / d_alpha
			* (h(alpha_idx + 1, q) - 2 * h(alpha_idx, q) - h(alpha_idx - 1, q))
			- phi * q * q + lambda_ask * max(Delta + I_operator<true>(alpha_idx, q - 1),
				I_operator<true>(alpha_idx, q)) + lambda_bid * max(Delta + I_operator<false>(alpha_idx, q + 1),
					I_operator<false>(alpha_idx, q)) - (lambda_ask + lambda_bid) * h(alpha_idx, q));
	}

	double M_operator(int64_t alpha_idx, int64_t q) {

		if (q + 1 > Q) {
			return h(alpha_idx, q - 1) - Upsilon;
		}

		if (q - 1 < -Q) {
			return h(alpha_idx, q + 1) - Upsilon;
		}

		return max(h(alpha_idx, q - 1), h(alpha_idx, q + 1)) - Upsilon;
	}

	double S_operator(int64_t alpha_idx, int64_t q) {
		return max(T_operator(alpha_idx, q), M_operator(alpha_idx, q));
	}

	void Step() {
		auto new_values = std::vector<std::vector<double>>(2 * Q + 1, std::vector<double>(n_a));

		for (int64_t alpha_idx = 0; alpha_idx < n_a; ++alpha_idx) {
			for (int64_t q = -Q; q <= Q; ++q) {
				new_values[q + Q][alpha_idx] = S_operator(alpha_idx, q);
			}
		}

		h_values.push_back(new_values);
	}

	void Run() {
		for (int64_t i = 0; i < n_t; ++i) {
			Step();
		}
	}

	void Log(std::ostream& out = std::cout) {

		for (int64_t t_idx = n_t; t_idx >= 0; --t_idx) {
			for (int64_t alpha_idx = 0; alpha_idx < n_a; ++alpha_idx) {
				for (int64_t q = -Q; q <= Q; ++q) {
					out << q << " " << -A + alpha_idx * 2 * A / (n_a - 1) << " " << h_values[t_idx][q + Q][alpha_idx] << std::endl;
				}
			}
			out << std::endl << std::endl;
		}
	}

	double Interpolate_h(int64_t t_idx, int64_t q, double alpha) {

		double frac = alpha / d_alpha;
		int alpha_idx_left = std::floor(frac) + (n_a - 1) / 2;
		int alpha_idx_right = std::ceil(frac) + (n_a - 1) / 2;
		double h_left, h_right;

		if (alpha_idx_left >= n_a) {
			double delta_h = h_values[t_idx][q][n_a - 1] - h_values[t_idx][q][n_a - 2];
			h_left = h_values[t_idx][q][n_a - 1] + (alpha_idx_left - n_a) * delta_h;
			h_right = h_left + delta_h;
		} 
		else if (alpha_idx_right <= 0) {
			double delta_h = h_values[t_idx][q][0] - h_values[t_idx][q][1];
			h_right = h_values[t_idx][q][0] - alpha_idx_right * delta_h;
			h_left = h_right + delta_h;
		}
		else {
			h_left = h_values[t_idx][q][alpha_idx_left];
			h_right = h_values[t_idx][q][alpha_idx_right];
		}
		
		return h_left + (h_right - h_left) * (frac - std::floor(frac));
	}

	void Log_csv(std::ostream& out = std::cout) {

		out << "t,q,alpha,h,l^a,l^b,tau^b,tau^a" << std::endl;

		bool l_ask = 0, l_bid = 0, tau_ask = 0, tau_bid = 0;

		for (int64_t t_idx = n_t; t_idx >= 0; --t_idx) {
			for (int64_t alpha_idx = 0; alpha_idx < n_a; ++alpha_idx) {
				for (int64_t q = -Q; q <= Q; ++q) {

					out << T * (1 - double(t_idx) / n_t)<< "," << q << "," << -A + alpha_idx * 2 * A / (n_a - 1) << "," << h_values[t_idx][q + Q][alpha_idx] << ",";
					
					if (q + 1 <= Q) {
						double alpha_minus = -A + alpha_idx * d_alpha - gamma_bid;

						l_bid = (Delta + Interpolate_h(t_idx, q + 1 + Q, alpha_minus)) > Interpolate_h(t_idx, q + Q, alpha_minus);
						tau_ask = (- Upsilon + h_values[t_idx][q + 1 + Q][alpha_idx]) > h_values[t_idx][q + Q][alpha_idx];
					}
					else {
						l_bid = 0;
						tau_ask = 0;
					}
					
					if (q - 1 >= -Q) {
						double alpha_plus = -A + alpha_idx * d_alpha + gamma_ask;

						l_ask = (Delta + Interpolate_h(t_idx, q - 1 + Q, alpha_plus)) > Interpolate_h(t_idx, q + Q, alpha_plus);
						tau_bid = (- Upsilon + h_values[t_idx][q - 1 + Q][alpha_idx]) > h_values[t_idx][q + Q][alpha_idx];
					}
					else {
						l_ask = 0;
						tau_bid = 0;
					}
					
					out << l_ask << "," << l_bid << "," << tau_bid << "," << tau_ask << std::endl;
				}
			}
		}
	}

	void PrintConfig() {
		std::cout << std::setw(3) << "n_t" << std::setw(3) << "= " << n_t << std::endl;
		std::cout << std::setw(3) << "n_a" << std::setw(3) << "= " << n_a << std::endl;
		std::cout << std::setw(3) << "Q" << std::setw(3) << "= " << Q << std::endl;
	}

	double max(double a, double b) {
		return a > b ? a: b;
	}

	double min(double a, double b) {
		return a < b ? a : b;
	}

private:

	std::vector<std::vector<std::vector<double>>> h_values;

	double T; // terminal time
	double A; // alpha signal boundary
	int Q; // inventory boundary

	double k, rho;
	double gamma_ask, gamma_bid;
	double lambda_ask, lambda_bid;
	
	double Delta; // half-spread
	double sigma; // tick size
	double Upsilon; // liquidity taking cost
	double phi; // running penalty coefficient
	double psi ; // LOB walking penalty

	int n_a, n_t; // number of points in grid
	double d_alpha, d_t; // grid step
};

int main() {

	Strategy strat(10.0, 300.0, 4, 200.0, 1.0, 60.0, 60.0,
		1.0, 1.0, 0.005, 0.01, 0.01, 1e-6, 0.0, 11);

	strat.Initialize();

	strat.PrintConfig();

	strat.Run();

	std::ofstream out("solution.csv");

	strat.Log_csv(out);

	return 0;
}
